import { Component, OnInit } from '@angular/core';
import { Liste } from 'src/model/liste';
import { DataService } from 'src/services/data-service';


@Component({
  selector: 'app-liste-index',
  templateUrl: './liste-index.component.html',
  styleUrls: ['./liste-index.component.scss']
})
export class ListeIndexComponent implements OnInit {

  constructor(private service: DataService) { }
   // a l instanciation la listedeliste es un defined
  listeDeliste: Liste[];


  ngOnInit() {

    // le getliste revoint la promesse
    this.service.getListes().then(resultat => {

      //quand la promesse  est resolue,
      // on    le resultat (la liqte des listes)
      //et le met dans
      this.listeDeliste = resultat;
    });
  }

}
