
import {Guid} from 'guid-typescript';
import { ListeItem } from './liste-items';

export class Liste {

 constructor( o?: any) {
   this.id = Guid.create();
   this.dateCreation = new Date();
   for(var p in o) {
      this[p] = o[p];
   }
   //this.description = description;

  }
  id: Guid;
  libelle: string = 'Nouvelle liste';
  theme: string;
  imageUrl: string;
  description: string;
  dateCreation: Date;
  nbItemsMax: 10;
  items: ListeItem[] = [];
}
