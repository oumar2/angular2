import { DataHttpService } from 'src/services/data-http.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { DataService } from 'src/services/data-service';

export const environment = {
  production: true,
  appName: 'Shopping liste',
  serviceUrl: 'http://www.monservice.com',
  providers: [
    { provide: DataService, useClass: DataHttpService },
    MessageService
  ]
};
